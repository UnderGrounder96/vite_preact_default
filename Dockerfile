# BASE
FROM oven/bun:alpine AS base
USER bun
WORKDIR /app
COPY --chown=bun .env* vite.config.js bun*.lockb package*.json ./
RUN bun install
VOLUME /app/node_modules


# DEV
FROM base AS dev
COPY --chown=bun src/ src/
COPY --chown=bun index.html ./
COPY --chown=bun public/ public/
CMD [ "run", "dev" ]


# PREVIEW
FROM base AS prev
RUN --mount=type=bind,source=src,target=src \
  --mount=type=bind,source=public,target=public \
  --mount=type=bind,source=index.html,target=index.html \
  bun run build
CMD [ "run", "preview" ]


# PROD
FROM nginx:alpine AS prod
COPY --from=prev /app/dist /usr/share/nginx/html
