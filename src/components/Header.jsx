export default function Header() {
  return (
    <header>
      <nav>
        <div class="section-center navbar">
          <h2>Navbar logo/title</h2>
          <div class="nav-links">
            <a href="#">Page 1</a>
            <a href="#">Page 2</a>
          </div>
        </div>
      </nav>
    </header>
  );
}
