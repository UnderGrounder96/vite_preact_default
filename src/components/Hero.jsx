export default function Hero() {
  return (
    <>
      <h1>Hello World</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat error
        tenetur, et obcaecati at ullam minima porro, est ratione inventore
        aliquid nobis, doloremque voluptate eveniet.
      </p>

      <h2>hello world</h2>
      <h3>hello world</h3>
      <h4>hello world</h4>
      <h5>hello world</h5>

      <a href="#">link example</a>

      <ul>
        <li>First Item</li>
        <li>Second Item</li>
        <li>Third Item</li>
      </ul>

      <button type="button">regular button</button>
      <button class="btn-funky" type="button">
        funky button
      </button>

      <br />
      <br />

      <div class="alert alert-danger">hello from danger</div>
      <div class="alert alert-success">hello from success</div>

      <form>
        <h4>contact form</h4>
        <div class="form-row">
          <label for="name">Name</label>
          <input
            type="text"
            name="name"
            id="name"
            placeholder="John Doe"
            autocomplete="on"
          />
        </div>
        <div class="form-row">
          <label for="number">Number</label>
          <input type="number" name="number" id="number" />
        </div>
        <div class="form-row">
          <label for="email">Email</label>
          <input type="email" name="email" id="email" autocomplete="on" />
          <small class="form-alert">please provide value</small>
        </div>
        <div class="form-row">
          <label for="textarea">Textarea</label>
          <textarea
            name="textarea"
            id="textarea"
            placeholder="Default Value"
            autocomplete="on"
          >
          </textarea>
        </div>
        <button type="submit" class="btn-block">
          submit
        </button>
      </form>

      <div class="loading"></div>

      <br />

      <div class="title">
        <h2>section title</h2>
        <div class="title-underline"></div>
      </div>
    </>
  );
}
