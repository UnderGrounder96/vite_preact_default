import {
  hydrate,
  LocationProvider,
  prerender as ssr,
  Route,
  Router,
} from "preact-iso";

import { Home } from "./pages/Home.jsx";
import { NotFound } from "./pages/404.jsx";

import Header from "./components/Header.jsx";
import Footer from "./components/Footer.jsx";

import "./styles/main.scss";

export function App() {
  return (
    <LocationProvider>
      <Header />
      <section class="main">
        <Router>
          <Route path="/" index component={Home} />
          <Route default component={NotFound} />
        </Router>
      </section>
      <Footer />
    </LocationProvider>
  );
}

if (typeof window !== "undefined") {
  hydrate(<App />, document.getElementById("app"));
}

export async function prerender(data) {
  return await ssr(<App {...data} />);
}
