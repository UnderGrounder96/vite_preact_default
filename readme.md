# vite_preact

## Why?

It's my vite + preactJS template project.

## Where?

https://undergrounder96.gitlab.io/vite_preact_default/

## How?

Manually:

```bash
# Install bun
curl -fsSL https://bun.sh/install | bash

# Install dependencies
bun install

# Run (development)
bun run dev

# Run (production)
bun run build
bun run preview
```

Docker:

```bash
# (with) docker-compose
docker-compose up
```

k8s:

```bash
# Build docker image
docker build preact-deploy .
docker push ... # push docker image to node/repository

# Deploy to k8s
kubectl apply -f k8s/
```
